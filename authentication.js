// Authentication requires ////////////////////////////////////////
///////////////////////////////////////////////////////////////////
var expressApp = require('./s5l-api').expressApp;
var express = require('express');
var router = express.Router();
var expressSession = require('express-session');
var cookieParser = require('cookie-parser');
var MongoDBStore = require('connect-mongodb-session')(expressSession);

var util = require('util');

var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
mongoose.Promise = require('bluebird');

var helpers = require('./helpers');
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

/// Auth Config ///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

var sessionStore = new MongoDBStore({
    uri: process.env.MONGO_URL,
    collection: 'stratusSessions'
});

sessionStore.on('error', function(error) {
    //assert.ifError(error);
    //assert.ok(false);
    console.log("Error occured in accessing sessionStore (MongoDB)...");
});

expressApp.enable('trust proxy'); // add this line
expressApp.use(cookieParser());
expressApp.use(expressSession({
    name: 'stratusCloudCookie',
    secret: '076ee61d63aa10a125ea872411e433b9',
    resave: true,
    saveUninitialized: true,
    proxy: true,
    rolling: true,
    unset: 'destroy',
    cookie: {
        secure: false,
        maxAge: 1000 * 60 * 60 * 24 * 7, // 1 week
        store: sessionStore
    },
    maxAge: new Date(Date.now() + 3600000),
    store: sessionStore
}));

// passport config 1 (DO BEFORE config 2)
expressApp.use(passport.initialize());
expressApp.use(passport.session());
// END config 1

// passport config 2 (DO AFTER config 1)
var Account = require('./models/account');
//passport.use(new LocalStrategy(Account.authenticate()));
passport.use(Account.createStrategy());
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());
// END config 2

//mongoose.connect(process.env.MONGO_URL);

mongoose.connect(process.env.MONGO_URL,{
    useMongoClient: true,
    poolSize: 5,
    socketTimeoutMS: 0,
    keepAlive: true,
    reconnectTries: 30,
    // user: process.env.DB_USER,
    // pass: process.env.DB_PASS,
}, function(err){
    if(err) return console.log(err);
    mongoose.Promise = global.Promise;
    console.log('DB Connection Established');
});

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////


/// Auth Functions ////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

function requiresAuth(req, res, next) {
    if (req.isAuthenticated()) return next();
    console.log(("requiresAuth() - NOT AUTHENTICATED").red);
    res.statusCode = 401;
    var json_resp = {};
    json_resp.status = 'failure';
    json_resp.loggedIn = false;
    if (req.method === 'GET') json_resp.returnTo = req.originalUrl;
    res.json(json_resp);
};

module.exports = {
    requiresAuth: requiresAuth,
    router: router
};

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////


/// Auth Setup ////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

router.all('*', helpers.setCorsHeaders);

router.get('/checkAuth', function (req, res) {
    if(req.isAuthenticated())
    {
        res.send({status:'success', loggedIn: true});
    } else {
        res.send({status:'failure', loggedIn: false});
    }
});

router.get('/getUser', function (req, res) {
    if(req.isAuthenticated())
    {
        res.send({status:'success', user: req.user});
    } else {
        res.send({status:'failure'});
    }
});

router.get('/checkAuth401', requiresAuth, function (req, res) {
    res.send({status:'success', loggedIn: true});
});

router.post('/authenticate', function (req, res) {
    passport.authenticate('local')(req, res, function () {
        //res.redirect('/view1');
        console.log(("/authenticate - AUTHENTICATED").green);
        //console.log(req);
        req.session.save(() => {
            res.statusCode = 200;
            res.json(util.inspect(res));
        });
    });
    console.log(("/authenticate - NOT AUTHENTICATED").red);
});

router.post('/authenticate2', function (req, res) {
    var next = function(req, res, err) {
        console.log(("/authenticate2 - NOT AUTHENTICATED").red);
        return res.send({ status:'failure' });
    };
    passport.authenticate('local', function(err, user, info) {
        if (err) { return next(req, res, err); }
        if (!user) { return next(req, res, err); }
        req.logIn(user, function(err) {
            if (err) { return next(req, res, err); }
            console.log(("/authenticate2 - AUTHENTICATED").green);
            return req.session.save((err) => {
                res.statusCode = 200;
                res.send({
                    status: 'success',
                    _passport: req._passport,
                    sessionID: req.sessionID,
                    session: req.session,
                    user: req.user
                });
            });
        });
    })(req, res, next);

});

router.post('/register', function(req, res) {
    Account.register(new Account({
            name: req.body.name,
            username: req.body.username,
            email: req.body.email,
            subDomain: req.body.subDomain,
            clientID: req.body.clientID,
            exisNodeAddr: req.body.exisNodeAddr,
        }),
        req.body.password,
        function(err, account) {
            if (err) {
                //return res.render('register', { account : account });
                return res.redirect('/getUser');
            }

            passport.authenticate('local')(req, res, function () {
                return res.redirect('/getUser');
            });
        });
});

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////