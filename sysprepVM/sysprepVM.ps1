<#
.SYNOPSIS
    Sysprep a VM for VHD image creation

.DESCRIPTION
    xxxxxxxxx

.EXAMPLE
   ./sysprepVM.ps1

#>

Start-Process -FilePath C:\Windows\System32\Sysprep\Sysprep.exe -ArgumentList '/generalize /oobe /shutdown /quiet'