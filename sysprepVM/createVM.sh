#!/bin/sh

# ./setVars.sh

../helperScripts/loginToAzure.sh

## Create resourceGroup + storageAccount ###############
########################################################

# pkgcreate0vmimages - key 1
# cvgUPzI+WgtvnwGjjxil+XiPJQbWcAYxTVosUFkA1CvWKxwkRo0DI3SCY3UNrHkk8x4H0o8Q+DxbpNaD4wWkrQ==


#az group create \
#    --name ${RESOURCE_GROUP} \
#    --location ${DATACENTER}

#az storage account create \
#    --resource-group ${RESOURCE_GROUP} \
#    --location ${DATACENTER} \
#    --name ${STORAGE_ACCT_NAME} \
#    --kind Storage \
#    --sku Standard_LRS

########################################################

#az storage account keys list \
#    --resource-group ${RESOURCE_GROUP} \
#    --account-name ${STORAGE_ACCT_NAME}
#
#az storage container create \
#    --account-name ${STORAGE_ACCT_NAME} \
#    --name ${STORAGE_CONTAINER_NAME}

########################################################
########################################################


## Create Managed Disk for VM ##########################
########################################################

#az storage blob upload  \
#    --account-name ${STORAGE_ACCT_NAME} \
#    --account-key ${STORAGE_ACCT_KEY} \
#    --container-name ${STORAGE_CONTAINER_NAME} \
#    --type page \
#    --file /path/to/disk/mydisk.vhd \
#    --name myDisk.vhd
#
#az disk create \
#    --resource-group ${RESOURCE_GROUP} \
#    --name "myManagedDisk" \
#    --sku Standard_LRS \
#    --source "https://vmimagessouthcentralus.blob.core.windows.net/imagehigh/imagehigh.vhd"
#
#az vm create \
#    --resource-group ${RESOURCE_GROUP} \
#    --location ${DATACENTER} \
#    --name ${VM_NAME} \
#    --os-type "windows" \
#    --attach-os-disk "myManagedDisk"

########################################################

#az vm create \
#    --resource-group ${RESOURCE_GROUP} \
#    --location ${DATACENTER} \
#    --name ${VM_NAME} \
#    --image "Win2016Datacenter"

export RESOURCE_GROUP="$1"
export DATACENTER="$2"
export VM_NAME="$3"

az group create \
    --name ${RESOURCE_GROUP} \
    --location ${DATACENTER}

az group deployment create \
    --name "${VM_NAME}deployment" \
    --resource-group ${RESOURCE_GROUP} \
    --template-file deploy.json \
    --parameters @deploy.params.json

# http://localhost:8000/api/package/createPackage