var express  = require('express');
var expressApp = express();
module.exports.expressApp = expressApp;

var morgan = require('morgan');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var methodOverride = require('method-override');
var http = require('http');
var path = require('path');
var querystring = require('querystring');

var expressSession = require('express-session');
var timeout = require('connect-timeout'); //express v4

var dotenv = require('dotenv');
dotenv.config();

var cors = require('cors');

/// Express ///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

// log every request to the console
expressApp.use(morgan('dev'));

//expressApp.use(cors());
//expressApp.options('*', cors());
expressApp.use(bodyParser.json()); // for parsing application/json
expressApp.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////


// Init Modules ///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

// Authentication to API endpoints and auth routes
var auth = require('./authentication');
expressApp.use('/', auth.router);

// Main Routing
var routes = require('./routes');
expressApp.use('/', routes);

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

// ==== listen (start app with nodemon s5l-api.js) =========

var httpPort = 7000;
expressApp.listen(httpPort, function(){
    console.log("Express HTTP Server listening on port " + httpPort);
});


// ===============================================================