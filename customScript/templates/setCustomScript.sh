#!/bin/sh

RESOURCE_GROUP="VAR_RESOURCE_GROUP"
VM_NAME="VAR_VM_NAME"

#PUBLIC_SCRIPT='{"fileUris":["https://'${STORAGE_ACCT_NAME}'.blob.core.windows.net'${SCRIPT_PATH}'"], "commandToExecute":".'${SCRIPT_PATH}'"}'
#PROTECTED_SCRIPT='{"storageAccountName":"'${STORAGE_ACCT_NAME}'", "storageAccountKey":"'${STORAGE_ACCT_KEY}'"}'

## Login to Azure ######################################
########################################################

../helperScripts/loginToAzure.sh

########################################################
########################################################


## Run Custom Script ###################################
########################################################

PUBLIC_SCRIPT="public.json"
PROTECTED_SCRIPT="protected.json"

az vm extension set \
	--resource-group ${RESOURCE_GROUP} \
	--vm-name ${VM_NAME} \
	--name CustomScriptExtension \
	--publisher Microsoft.Compute --version 1.9 \
	--settings ${PUBLIC_SCRIPT} \
	--protected-settings ${PROTECTED_SCRIPT} \
	--debug

########################################################
########################################################

#az vm extension list -g ${RESOURCE_GROUP} --vm-name ${VM_NAME}
#az vm extension list -g teamsdemo1 --vm-name teamsdemo1app0

## Set-Location -Path /var/log/azure/custom-script/handler.log
