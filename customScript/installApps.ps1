#net use W: \\thirdpartysoftwares.file.core.windows.net\software /u:thirdpartysoftwares iCtU305iytDt53ttlpPcr3QvSEDUHID8+RuUtSfJSHtvn5sMwU6jteWoyCo9Q8uikF+3DmGHRBa6LeKCP/DGuA==
#Copy-Item  "W:\adobe1.zip" -Destination "C:\" -Recurse
#$appsToInstall = _APPS_
#$appsToInstall = "adobepp.ps1","ansys.ps1","autocad.ps1","dropbox.ps1","matlab.ps1"
#$appsToInstall = "adobepp.ps1","dropbox.ps1","matlab.ps1"
$appsToInstall = "dropbox.ps1"
#function Expand-ZIPFile($file, $destination)
#{
#    $shell = new-object -com shell.application
#    $zip = $shell.NameSpace($file)
#    foreach($item in $zip.items())
#    {
#        $shell.Namespace($destination).copyhere($item)
#    }
#}

$CmdMessage = {C:\windows\system32\msg.exe * "Starting script execution..."}
$CmdMessage | Invoke-Expression

Set-ExecutionPolicy Unrestricted

foreach($app in $appsToInstall)
{
    #Copy-Item  "W:\$app" -Destination "C:\" -Recurse
    Invoke-WebRequest https://thirdpartysoftwares.blob.core.windows.net/software/$app -UseBasicParsing -OutFile $app
    #Invoke-WebRequest "https://thirdpartysoftwares.blob.core.windows.net/software/$app" -OutFile "C:\$app"

    # Exec $app script
    Invoke-Expression powershell -ExecutionPolicy Unrestricted -File $app

    $CmdMessage = {C:\windows\system32\msg.exe * "Script $app has been executed!"}
    $CmdMessage | Invoke-Expression
    Start-Sleep -s 5
}

#net use W: /delete