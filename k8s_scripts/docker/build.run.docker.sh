#!/bin/sh

. scripts_setup.sh

. clean.docker.sh

#docker login -u $APP_ID -p $APP_SECRET_PASSWORD $CURRENT_STRATUS_DOCKER_REGISTRY
docker build --no-cache -t $PROJECT_NAME:1 ../../
#docker tag $PROJECT_NAME:1 $CURRENT_STRATUS_DOCKER_REGISTRY/$PROJECT_NAME:1
#docker push $CURRENT_STRATUS_DOCKER_REGISTRY/$PROJECT_NAME:1
#kubectl create -f ../../kubernetes/azure/

docker run -d --restart=on-failure -e TCP_PORTS=7000 -p 7000:7000 --name=$PROJECT_NAME -t $PROJECT_NAME:1