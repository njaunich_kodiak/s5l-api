param (
    [Parameter(Mandatory=$true)]
    [String]$sourceResourceGroupName,

    [Parameter(Mandatory=$true)]
    [String]$sourceVmName,

    [Parameter(Mandatory=$true)]
    [String]$sourceRegion,

    [Parameter(Mandatory=$true)]
    [String]$targetResourceGroupName,

    [Parameter(Mandatory=$true)]
    [String]$targetStorageAccountName,

    [Parameter(Mandatory=$true)]
    [String]$targetImageContainerName,

    [Parameter(Mandatory=$true)]
    [String]$targetImageBlobName,

    [Parameter(Mandatory=$true)]
    [String]$snapshotAccessSasUri

)

$appId = "3fc44304-5066-4204-bbac-feecc9d2dbf5"
$appSecretPassword = "52401101-1cf0-4fcb-af82-e6371865b1c9"
$tenantId = "c648dcad-f07a-4660-b409-2d57cef5a30a"

$SecurePassword = ConvertTo-SecureString -String $appSecretPassword -AsPlainText -Force

# Convert Service Principal variables into Credential for Login-AzureRmAccount
$credential = New-Object -TypeName System.Management.Automation.PSCredential($appId, $SecurePassword)

# Login to Azure using Service Principal
#Login-AzureRmAccount -ServicePrincipal -ApplicationId  $appId -Credential $credential -TenantId $tenantId
#Clear-AzureProfile –Force
Login-AzureRmAccount -Credential $credential -Tenant $tenantId -ServicePrincipal

#$job = (Get-AzureRmAutomationJob –AutomationAccountName "General-Automation-Account" `
#    –RunbookName "createVMSnapshot" -ResourceGroupName "Automation" | sort LastModifiedDate –desc)[0]
##Get-AzureRmAutomationJobOutput -ResourceGroupName "Automation" `
##    –AutomationAccountName "General-Automation-Account" -Id $job.JobId –Stream Output
#
## $job = (Get-AzureRmAutomationJob –AutomationAccountName "General-Automation-Account" –RunbookName "createVMSnapshot" -ResourceGroupName "Automation" | sort LastModifiedTime –desc)[0]
#
#do {
#    $job = (Get-AzureRmAutomationJob –AutomationAccountName "General-Automation-Account" `
#        –RunbookName "createVMSnapshot" -ResourceGroupName "Automation" | sort LastModifiedTime –desc)[0]
#    Write-Output "createVMSnapshot status = "
#    Write-Output $job.Status
#    Start-Sleep -s 10
#}until($job.Status -eq "Completed" -Or $job.Status -eq "Stopped" -Or $job.Status -eq "Failed")

#if ($job.Status -eq "Failed" -Or $job.Status -eq "Stopped")
#{
#    Write-Output "createVMSnapshot Runbook job failed!!"
#    exit
#}

$targetStorageContext = (Get-AzureRmStorageAccount -ResourceGroupName $targetResourceGroupName -Name $targetStorageAccountName).Context

$storageContainer = Get-AzureStorageContainer -Context $targetStorageContext -ErrorAction Stop | where-object {$_.Name -eq $targetImageContainerName}
If(!$storageContainer)
{
    New-AzureStorageContainer -Name $targetImageContainerName -Context $targetStorageContext -Permission Container
}

$blob = Get-AzureStorageBlob -Context $targetStorageContext -Container $targetImageContainerName -ErrorAction Stop | where-object {$_.Name -eq $targetImageBlobName}
If(!$blob)
{
    # Use the SAS URL to copy the blob to the target storage account (and thus region)
    Start-AzureStorageBlobCopy -AbsoluteUri $snapshotAccessSasUri -DestContainer $targetImageContainerName -DestContext $targetStorageContext -DestBlob $targetImageBlobName
    Get-AzureStorageBlobCopyState -Container $targetImageContainerName -Blob $targetImageBlobName -Context $targetStorageContext -WaitForComplete
    $osDiskVhdUri = ($targetStorageContext.BlobEndPoint + $targetImageContainerName + "/" + $targetImageBlobName)
    Write-Output $osDiskVhdUri
}
else {
    $osDiskVhdUri = ($targetStorageContext.BlobEndPoint + $targetImageContainerName + "/" + $targetImageBlobName)
    Write-Output $osDiskVhdUri
}
