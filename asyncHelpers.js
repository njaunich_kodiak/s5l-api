var http = require('http');
var querystring = require('querystring');
var packageCreate = require('./packageCreate');
var apiCalls = require('./apiCalls');

module.exports = {
    installCustomApps: installCustomApps,
    sysprepVM: sysprepVM,
    generalizeVM: generalizeVM,
    copyVHDToSA: copyVHDToSA,
    createPackageWorkflow: createPackageWorkflow,
    sync_createPackageWorkflow: sync_createPackageWorkflow
};

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function installCustomApps(request,callback) {

    console.log('setting VM for customScript ...');
    packageCreate.setVMForCustomScript(request.resourceGroup, request.vmName);

    var scriptsArray = [];
    if (request.appsToInstall) {
        if (request.appsToInstall.length !== 0) {
            for (var i=0; i < request.appsToInstall.length; i++) {
                scriptsArray.push({'name': request.appsToInstall[i].name+'.ps1', 'path': '', 'shouldUploadAsBlob': false});
            }
        }
    }


    await packageCreate.execCustomBlobScripts(scriptsArray, function(err,result) {
        if (err) return callback(err,null);
        else return callback(null,result);
    });

    return;
};

async function sysprepVM(request,callback) {

    console.log('sysprep-ing VM ...');
    packageCreate.sysprepVM(request.resourceGroup, request.vmName, function(err,result) {
        if (err) return callback(err,null);
        else return callback(null,result);
    });

    return;
};

async function generalizeVM(request,callback) {

    console.log('generalizing VM ...');
    packageCreate.execGeneralizeVM(request, function(err,result) {
        if (err) return callback(err,null);
        else return callback(null,result);
    });

    return;
};

async function copyVHDToSA(request,callback) {

    console.log('copying generalized VHD to images Storage Account ...');

    console.log("uploading copyImage.ps1 as file blob ...");
    packageCreate.uploadScriptAsBlob("copyImage.ps1", "sysprepVM/copyImage.ps1", 0, function(err,result) {
        if (err) return callback(err,null);
        else {
            console.log("uploaded copyImage.ps1 script as blob!");

            console.log("uploading createVMSnapshot.ps1 as file blob ...");
            packageCreate.uploadScriptAsBlob("createVMSnapshot.ps1", "sysprepVM/createVMSnapshot.ps1", 0, function(err,result) {
                if (err) return callback(err,null);
                else {
                    console.log("uploaded createVMSnapshot.ps1 script as blob!");
                    apiCalls.createDisk(request, function (err, result) {
                        if (err) callback(err,null);
                        else return callback(null,result);
                    });
                }
            });
        }
    });

    return;
};

async function createPackageWorkflow(request,funcName,shouldSleep) {
    // create callback function to pass into sync_createPackageWorkflow
    var callback = function(err,result) {
        if (err) {
            module.exports.packageCreateJobs[request.jobId].status = "failed";
            module.exports.packageCreateJobs[request.jobId].error = err;
            var stage = module.exports.packageCreateJobs[request.jobId].stage;
            console.log('Error occured at stage "' + stage + '"');
            console.log("Error = " + err);
        }
        else if (result) {
            console.log("Successful workflow callback exec!!");
        } else {
            console.log("This should never occur ...");
        }
    };

    await sync_createPackageWorkflow(request,funcName,callback,shouldSleep);
};

function sync_createPackageWorkflow(request,funcName,callback,shouldSleep) {
    if (!shouldSleep) switch (funcName) {
        case "execCreateVM":
            packageCreate.execCreateVM(request.resourceGroup, request.datacenter, request.vmName, function(err,result) {
                if (err) callback(err,null);
                else sync_createPackageWorkflow(request,funcName,callback,true);
            });
            break;
        case "installCustomApps":
            installCustomApps(request,callback).then(v => {
                sync_createPackageWorkflow(request,funcName,callback,true);
            });
            break;
        case "sysprepVM":
            sysprepVM(request,callback).then(v => {
                sync_createPackageWorkflow(request,funcName,callback,true);
            });
            break;
        case "generalizeVM":
            generalizeVM(request,callback).then(v => {
                sync_createPackageWorkflow(request,funcName,callback,true);
            });
            break;
        case "copyVHDToSA":
            copyVHDToSA(request,callback).then(v => {
                sync_createPackageWorkflow(request,funcName,callback,true);
            });
            break;
        case "workflowDone":
            if (packageCreate.packageCreateJobs[request.jobId].status !== "failed")
                packageCreate.packageCreateJobs[request.jobId].status = "finished";
            break;
    }
    else if (shouldSleep) switch (funcName) {
        case "execCreateVM":
            sleep(request.waitTimes.afterCreateVM * 60 * 1000).then(v => {
                if (packageCreate.packageCreateJobs[request.jobId].status === "failed") return;
                packageCreate.packageCreateJobs[request.jobId].stage = "/installCustomApps";
                sync_createPackageWorkflow(request,"installCustomApps",callback,false);
            });
            break;
        case "installCustomApps":
            sleep(request.waitTimes.afterInstallCustomApps * 60 * 1000).then(v => {
                if (packageCreate.packageCreateJobs[request.jobId].status === "failed") return;
                packageCreate.packageCreateJobs[request.jobId].stage = "/sysprepVM";
                sync_createPackageWorkflow(request,"sysprepVM",callback,false);
            });
            break;
        case "sysprepVM":
            sleep(request.waitTimes.afterSysprepVM * 60 * 1000).then(v => {
                if (packageCreate.packageCreateJobs[request.jobId].status === "failed") return;
                packageCreate.packageCreateJobs[request.jobId].stage = "/generalizeVM";
                sync_createPackageWorkflow(request,"generalizeVM",callback,false);
            });
            break;
        case "generalizeVM":
            sleep(request.waitTimes.afterGeneralizeVM * 60 * 1000).then(v => {
                if (packageCreate.packageCreateJobs[request.jobId].status === "failed") return;
                packageCreate.packageCreateJobs[request.jobId].stage = "/copyVHDToSA";
                sync_createPackageWorkflow(request,"copyVHDToSA",callback,false);
            });
            break;
        case "copyVHDToSA":
            sleep(request.waitTimes.afterCopyVHDToSA * 60 * 1000).then(v => {
                if (packageCreate.packageCreateJobs[request.jobId].status === "failed") return;
                packageCreate.packageCreateJobs[request.jobId].stage = "workflowDone";
                sync_createPackageWorkflow(request,"workflowDone",callback,false);
            });
            break;
    }

    // After "workflowDone"
    return;
};