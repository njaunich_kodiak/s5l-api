#!/bin/sh

export RESOURCE_GROUP="pkgcreate0"
export DATACENTER="southcentralus"

export VM_NAME="pkgcreate0vm0"

export STORAGE_ACCT_NAME="pkgcreate0vmimages"
export STORAGE_CONTAINER_NAME="pkgcreate0vmimages"