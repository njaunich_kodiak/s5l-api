#!/bin/sh

## Login to Azure ######################################
########################################################

./loginToAzure.sh

########################################################
########################################################

## Upload Script #######################################
########################################################

STORAGE_ACCT_NAME="thirdpartysoftwares"
STORAGE_ACCT_KEY="iCtU305iytDt53ttlpPcr3QvSEDUHID8+RuUtSfJSHtvn5sMwU6jteWoyCo9Q8uikF+3DmGHRBa6LeKCP/DGuA=="

SCRIPT_UPLOAD_NAME="$1"
SCRIPT_PATH="$2"

# Script file path is relative to packageCreation directory
# e.g script.ps1 inside customScript directory has path of "customScript/script.ps1"

az storage blob upload --container-name software \
    --file ../${SCRIPT_PATH} \
    --name ${SCRIPT_UPLOAD_NAME} \
    --account-key ${STORAGE_ACCT_KEY} \
    --account-name ${STORAGE_ACCT_NAME}

########################################################
########################################################