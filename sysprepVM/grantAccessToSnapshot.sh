#!/bin/sh

## Login to Azure ######################################
########################################################

../helperScripts/loginToAzure.sh

########################################################
########################################################

## Grant Access to Snapshot ############################
########################################################

DURATION=$1
SNAPSHOT_NAME="$2"
RESOURCE_GROUP="$3"

az snapshot grant-access \
    --duration-in-seconds ${DURATION} \
    --name ${SNAPSHOT_NAME} \
    --resource-group ${RESOURCE_GROUP}

