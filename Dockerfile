FROM azure-cli-python:2.0.24-stratus
#FROM azuresdk/azure-cli-python

## Install Nodejs ######################################
########################################################

RUN apk update && apk add nodejs=8.9.3-r0

########################################################
########################################################

WORKDIR /usr/src/app

# Copy app dependencies
COPY package.json .
COPY package-lock.json .

# Bundle app source
COPY . .

# Install node dependencies
RUN npm install

CMD node s5l-api.js