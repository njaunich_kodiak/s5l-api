var express  = require('express');

var powershell = require('node-powershell');

const { exec } = require('child_process');

var fs = require('fs');
var replace = require('replace');

// Functions //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

function runPowershell(command,variables,callback) {

    var ps = new shell({executionPolicy: 'Bypass', debugMsg: true, noProfile: true});

    ps.addCommand(command,variables).then(function(){
        return ps.invoke();
    }).then(function(output){
        callback(null,output);
        ps.dispose();
    }).catch(function(err){
        console.log(err);
        callback(err,null);
        ps.dispose();
    });
}

var callback = function (err,result) {
    if (err) console.log(err);
    else console.log(result);
};

function run() {
    var command = './test.ps1';
    var variables = [{name:'var1' ,value:1},{name:'var2' ,value:2}];

    console.log("run()");
}

function setVMForCustomScript(resourceGroup, vmName) {
    var data = fs.readFileSync('customScript/templates/setCustomScript.sh', 'utf8');
    var data2 = data.replace(/VAR_VM_NAME/g, '' + vmName);
    var result = data2.replace(/VAR_RESOURCE_GROUP/g, '' + resourceGroup);
    fs.writeFileSync('customScript/setCustomScript.sh', result, 'utf8');
}

function createDefaultPublicDotJson() {
    var data = fs.readFileSync('customScript/templates/public.json', 'utf8');
    var data2 = data.replace(/VERSION_TIMESTAMP/g, '' + Date.now());
    var result = data2.replace(/SCRIPT_FILENAME/g, 'script.ps1');
    fs.writeFileSync('customScript/public.json', result, 'utf8');
}

function setScriptMessage() {
    var data = fs.readFileSync('customScript/templates/script.ps1', 'utf8');
    var result = data.replace(/SCRIPT_MESSAGE/g, 'This is a new test! ' + Date.now());
    fs.writeFileSync('customScript/script.ps1', result, 'utf8');

    /* fs.readFile('customScript/default_script.ps1', 'utf8', function (err,data) {
        if (err) callback(err,null);

        var result = data.replace(/SCRIPT_MESSAGE/g, 'This is a new test! ' + Date.now());

        fs.writeFile('customScript/script.ps1', result, 'utf8', function (err) {
            if (err) callback(err,null);
            else callback(null,true);
        });
    }); */
}

function setScriptFromFile(path_to_script) {
    var data = fs.readFileSync(path_to_script, 'utf8');
    fs.writeFileSync('customScript/script.ps1', data, 'utf8');
}

function setScriptToBlob(script_name) {
    var data = fs.readFileSync('customScript/templates/public.json', 'utf8');
    var data2 = data.replace(/VERSION_TIMESTAMP/g, '' + Date.now());
    var result = data2.replace(/SCRIPT_FILENAME/g, script_name);
    fs.writeFileSync('customScript/public.json', result, 'utf8');
}

function setVMParams(request) {

    var dnsPrefix = request.resourceGroup;

    // Change deploy.json params //////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    var data = fs.readFileSync('sysprepVM/templates/deploy.json', 'utf8');
    data = data.replace(/IMAGE_NAME/g, '' + request.imageName);
    data = data.replace(/VM_SIZE/g, '' + request.vmSize);
    var result = data.replace(/_VM_NAME_/g, '' + request.vmName);
    fs.writeFileSync('sysprepVM/deploy.json', result, 'utf8');
    ///////////////////////////////////////////////////////////////////////////

    // Change deploy.params.json params ///////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    var data = fs.readFileSync('sysprepVM/templates/deploy.params.json', 'utf8');
    data = data.replace(/ADMIN_USERNAME/g, '' + request.adminUsername);
    data = data.replace(/ADMIN_PASSWORD/g, '' + request.adminPassword);
    data = data.replace(/DNS_PREFIX/g, '' + dnsPrefix);
    data = data.replace(/OS_TYPE/g, '' + request.osType);
    data = data.replace(/OS_DISK_URL/g, '' + request.osDiskUrl);
    var result = data.replace(/WINDOWS_OS_VERSION/g, '' + request.windowsOSVersion);
    fs.writeFileSync('sysprepVM/deploy.params.json', result, 'utf8');
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
}

function execSetCustomScript(scriptName, scriptNumber, callback) {
    exec('cd customScript && ./setCustomScript.sh', (err, stdout, stderr) => {
        if (err) callback(`${err}`,null);
        else callback(null,{stdout:`setCustomScript.sh STDOUT: ${stdout}`,
            scriptName: scriptName,
            scriptNumber: scriptNumber});
    });
}

function uploadScriptAsBlob(scriptName, scriptPath, scriptNumber, callback) {
    exec('cd helperScripts && ./uploadScriptAsBlob.sh ' + scriptName + ' ' + scriptPath, (err, stdout, stderr) => {
        if (err) callback(`${err}`,null);
        else callback(null,{stdout:`uploadScriptAsBlob.sh STDOUT: ${stdout}`,
            scriptName: scriptName,
            scriptNumber: scriptNumber});
    });
}

function execCreateVM(resourceGroup, datacenter, vmName, callback) {
    exec('cd sysprepVM && ./createVM.sh ' +
        resourceGroup + ' ' +
        datacenter + ' ' +
        vmName, (err, stdout, stderr) => {
        if (err) callback(`${err}`,null);
        else callback(null,{stdout:`createVM.sh STDOUT: ${stdout}`});
    });
}

function execGeneralizeVM(request, callback) {
    exec('cd sysprepVM && ./generalizeVM.sh ' +
        request.resourceGroup + ' ' +
        request.vmName + ' ' +
        request.vmImageName + ' ' +
        request.diskName, (err, stdout, stderr) => {
        if (err) callback(`${err}`,null);
        else callback(null,{stdout:`generalizeVM.sh STDOUT: ${stdout}`});
    });
}

function sysprepVM(resourceGroup, vmName, callback) {

    setScriptToBlob("sysprepVM.ps1");

    setVMForCustomScript(resourceGroup, vmName);

    uploadScriptAsBlob("sysprepVM.ps1", "sysprepVM/sysprepVM.ps1", 0, function(err,result) {
        if (err) callback(err,null);
        else {
            execSetCustomScript("sysprepVM.ps1", 0, function (err, result) {
                if (err) callback(err,null);
                else callback(null,result);
            });
        }
    });
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


async function execCustomBlobScripts(scripts,callback) {
    if (scripts.length === 0) return callback(null,true);
    var i = 0;
    var currentScriptNum = -1;
    while (i < scripts.length) {
        if (i !== currentScriptNum) {
            console.log('setting script ' + i + ' by blob ...');
            setScriptToBlob(scripts[i].name);
        } else {
            console.log('waiting for script ' + i + ' to complete ...');
        }

        if (i !== currentScriptNum && scripts[i].shouldUploadAsBlob === true) {
            console.log("uploading custom script ...");
            uploadScriptAsBlob(scripts[i].name, scripts[i].path, i, function(err,result) {
                if (err) return callback(err,null);
                else {
                    console.log('running setCustomScript.sh ...');
                    execSetCustomScript(result.scriptName, result.scriptNumber, function (err, result) {
                        if (err) return;
                        else {
                            //i++;
                            console.log("finished " + result.scriptName + " script execution!");
                            //if (i === scripts.length) return callback(null,true);
                        }
                    });
                    currentScriptNum = i;
                    if (result.scriptNumber + 1 === scripts.length) return callback(null,true);
                }
            });
        } else if (i !== currentScriptNum) {
            console.log('running setCustomScript.sh ...');
            execSetCustomScript(scripts[i].name, i, function (err, result) {
                if (err) return;
                else {
                    //i++;
                    console.log("finished " + result.scriptName + " script execution!");
                    //if (i === scripts.length) return callback(null,true);
                }
            });
            currentScriptNum = i;
            if (i + 1 === scripts.length) return callback(null,true);
        }
        await sleep(15000);
        i++;
    }
}

var packageCreateJobs = {};

async function printJobStatus(jobId) {
    while(packageCreateJobs[jobId].status !== "finished" && packageCreateJobs[jobId].status !== "failed") {
        console.log('------------------------------');
        console.log('jobId: "'+jobId+'" = ');
        console.log(packageCreateJobs[jobId]);
        await sleep(60 * 1000); // seconds * (sec/ms)
    }
}

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

module.exports = {
    setVMForCustomScript: setVMForCustomScript,
    setScriptFromFile: setScriptFromFile,
    setScriptToBlob: setScriptToBlob,
    setVMParams: setVMParams,
    execSetCustomScript: execSetCustomScript,
    uploadScriptAsBlob: uploadScriptAsBlob,
    execCreateVM: execCreateVM,
    execGeneralizeVM: execGeneralizeVM,
    sysprepVM: sysprepVM,
    sleep: sleep,
    execCustomBlobScripts: execCustomBlobScripts,
    printJobStatus: printJobStatus,
    packageCreateJobs: packageCreateJobs
};