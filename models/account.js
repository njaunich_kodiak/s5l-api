var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

// var Account = new Schema({
//     username: String,
//     password: String,
//     clientID: String,
//     exisNodeAddr: String,
// });

var Account = new Schema({
    name: {
        type: String,
        required: true
        //unique:constants.TRUE
    },
    username : {
        type: String,
        required: true,
        unique: true
    },
    email:{
        type: String,
        required: true,
        unique: true
    },
    subDomain:{
        type: String,
        required: true
    },
    password: String,
    clientID: String,
    exisNodeAddr: String
});

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', Account);