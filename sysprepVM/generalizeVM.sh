#!/bin/sh

## Login to Azure ######################################
########################################################

../helperScripts/loginToAzure.sh

########################################################
########################################################

## Generalize VM #######################################
########################################################

RESOURCE_GROUP="$1"
VM_NAME="$2"

IMAGE_NAME="$3"
DISK_NAME="$4"

az vm deallocate \
    --resource-group ${RESOURCE_GROUP} \
    --name ${VM_NAME}

az vm generalize \
    --resource-group ${RESOURCE_GROUP} \
    --name ${VM_NAME}

az image create \
    --resource-group ${RESOURCE_GROUP} \
    --name ${IMAGE_NAME} \
    --source ${VM_NAME}

#az disk create \
#    --resource-group ${RESOURCE_GROUP} \
#    --name  ${DISK_NAME} \
#​    --source "493800c7-73a4-4b3c-9d58-c47a875e5698/resourceGroups/${RESOURCE_GROUP}/providers/Microsoft.Compute/images/${IMAGE_NAME}"

#az disk create \
#    --resource-group pkgcreate2 \
#    --name pkgcreate2vm0d \
#​    --source "/subscriptions/493800c7-73a4-4b3c-9d58-c47a875e5698/resourceGroups/pkgcreate2/providers/Microsoft.Compute/images/pkgcreate2vm0img0"
#
#az disk create \
#    --resource-group "pkgcreate2" \
#    --name "pkgcreate2vm0disk0" \
#​    --source "/subscriptions/493800c7-73a4-4b3c-9d58-c47a875e5698/resourceGroups/pkgcreate2/providers/Microsoft.Compute/disks/pkgcreate2vm0_disk1_09bbbc683d4443079831db2fd25c731e"
