module.exports.setCorsHeaders = function(req, res, next) {
    var headers = {};
    // IE8 does not allow domains to be specified, just the *
    // headers["Access-Control-Allow-Origin"] = req.headers.origin;
    var origin = req.get('origin');
    var host = req.get('host');
    var allowOrigin = origin ? origin : host;
    console.log("allowOrigin = "+allowOrigin);
    headers["Access-Control-Allow-Origin"] = allowOrigin;
    headers["Access-Control-Allow-Methods"] = 'PUT, GET, POST, DELETE, OPTIONS';
    headers["Access-Control-Allow-Credentials"] = true;
    headers["Access-Control-Max-Age"] = '86400'; // 24 hours
    headers["Access-Control-Allow-Headers"] = 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization';

    //intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
        //respond with 200
        console.log('OPTIONS request');
        res.writeHead(200, headers);
        res.end();
    }
    else {
        res.header('Access-Control-Allow-Origin', allowOrigin);
        res.header('Access-Control-Allow-Methods', headers["Access-Control-Allow-Methods"]);
        //make sure you set this parameter and make it true so that AngularJS and Express are able to exchange session values between each other
        res.header("Access-Control-Allow-Credentials", headers["Access-Control-Allow-Credentials"]);
        res.header("Access-Control-Max-Age", headers["Access-Control-Max-Age"]); // 24 hours
        res.header("Access-Control-Allow-Headers", headers["Access-Control-Allow-Headers"]);
        //move on
        next();
    }
};
