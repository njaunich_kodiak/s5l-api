var rest = require('restler');
var uuid = require('uuid');
const { exec } = require('child_process');

var azureAuth = require('./azure/azure-token/token');
/* Reference API versions
 * var api_ver = "2016-02-01";
 * var rgapi_ver = "2015-01-01";
*/

module.exports.generalizeVM = function (request, callback) {
    var api_ver = "2016-04-30-preview";
    azureAuth.getAuthToken(function(err, token) {
        if (err) callback(err, null);
        else {
            var uri = "https://management.azure.com/subscriptions/" + azureAuth.subId +
                "/resourceGroups/" + request.resourceGroup +
                "/providers/Microsoft.Compute/virtualMachines/" + request.vmName +
                "/generalize?api-version=" + api_ver;

            var jsonData = {};

            console.log(jsonData);

            var options = {
                accessToken: token.accessToken
            };

            // Do Rest POST request
            rest.postJson(uri, jsonData, options)
                .on('fail', function(data, response) {
                    callback(data, null);
                })
                .on('success', function(data, response) {
                    callback(null, data);
                });
        }
    });
};

function updateRunbook(request, options, callback) {
    var api_ver = "2015-10-31";

    var uri = "https://management.azure.com/subscriptions/" + azureAuth.subId +
        "/resourcegroups/" + request.resourceGroupName +
        "/providers/Microsoft.Automation/automationAccounts/" + request.automationAccountName +
        "/runbooks/" + request.runbookName +
        "?api-version=" + api_ver;

    var dataJson = {
        "properties": {
            "logVerbose": true,
            "logProgress": true,
            "runbookType": "PowerShell",
            "publishContentLink": {
                "uri": request.runbookScriptUri,
                "version": ""+Date.now()
            }
        },
        "name": request.runbookName,
        "location": "Southeast Asia"
    };

    rest.putJson(uri, dataJson, options)
        .on('fail', function(data, response) {
            callback(data, null);
        })
        .on('success', function(data, response) {
            callback(null, data);
        })
}

function createVMSnapshotWithRunbook(request, options, callback) {
    var api_ver = "2015-10-31";

    var runbookInput = {};
    runbookInput.resourceGroupName = "Automation",
        runbookInput.automationAccountName = "General-Automation-Account",
        runbookInput.uuid1 = uuid.v4(),
        runbookInput.runbookName="createVMSnapshot";

    var uri = "https://management.azure.com/subscriptions/" + azureAuth.subId +
        "/resourcegroups/" + runbookInput.resourceGroupName +
        "/providers/Microsoft.Automation/automationAccounts/" + runbookInput.automationAccountName +
        "/Jobs/" + runbookInput.uuid1 +
        "?api-version=" + api_ver;

    var dataJson = {
        "properties":{
            "runbook":{
                "name": runbookInput.runbookName
            },
            "parameters": request
        }
    };

    rest.putJson(uri, dataJson, options)
        .on('fail', function(data, response) {
            callback(data, null);
        })
        .on('success', function(data, response) {
            console.log("Runbook exec success!");
            callback(null, data);
        })
}

function copyVHDBlobWithRunbook(request, options, callback) {
    var api_ver = "2015-10-31";

    var runbookInput = {};
    runbookInput.resourceGroupName = "Automation",
        runbookInput.automationAccountName = "General-Automation-Account",
        runbookInput.uuid1 = uuid.v4(),
        runbookInput.runbookName="copyImage";

    var uri = "https://management.azure.com/subscriptions/" + azureAuth.subId +
        "/resourcegroups/" + runbookInput.resourceGroupName +
        "/providers/Microsoft.Automation/automationAccounts/" + runbookInput.automationAccountName +
        "/Jobs/" + runbookInput.uuid1 +
        "?api-version=" + api_ver;

    var dataJson = {
        "properties":{
            "runbook":{
                "name": runbookInput.runbookName
            },
            "parameters": request
        }
    };

    rest.putJson(uri, dataJson, options)
        .on('fail', function(data, response) {
            callback(data, null);
        })
        .on('success', function(data, response) {
            console.log("Runbook exec success!");

            var blobUri = "https://" + request.targetStorageAccountName +
                ".blob.core.windows.net/" + request.targetImageContainerName +
                "/" + request.targetImageBlobName;

            callback(null, {data:data,blobUri: blobUri});
        })
}

// function grantAccessToSnapshot(request, options, callback) {
//     //var api_ver = "2016-04-30-preview";
//     var api_ver = "2017-03-30";
//
//     var uri = "https://management.azure.com/subscriptions/" + azureAuth.subId +
//         "/resourceGroups/" + request.resourceGroupName +
//         "/providers/Microsoft.Compute/disks/" + request.snapshotName +
//         "/BeginGetAccess?api-version=" + api_ver;
//
//     var dataJson = {
//         "access": "read",
//         "durationInSeconds": 3600
//     };
//
//     rest.postJson(uri, dataJson, options)
//         .on('fail', function(data, response) {
//             callback(data, null);
//         })
//         .on('success', function(data, response) {
//             callback(null, data);
//         })
// }

function grantAccessToSnapshot2(request, callback) {

    exec('cd sysprepVM && ./grantAccessToSnapshot.sh ' +
        request.duration + ' ' +
        request.snapshotName + ' ' +
        request.resourceGroupName, (err, stdout, stderr) => {
        if (err) callback(`${err}`,null);
        else callback(null,{stdout:`${stdout}`});
    });
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports.createDisk = function (request, callback) {
    azureAuth.getAuthToken(function(err, token) {
        if (err) callback(err, null);
        else {

            var getVMInfoUri = "https://management.azure.com/subscriptions/" + azureAuth.subId +
                "/resourceGroups/" + request.resourceGroup +
                "/providers/Microsoft.Compute/virtualMachines/" + request.vmName +
                "?api-version=" + "2017-03-30";

            var options = {
                accessToken: token.accessToken
            };

            var createVMDiskWorkflow = function(data, callback) {
                console.log(data);
                var sourceResourceId = data.properties.storageProfile.osDisk.managedDisk.id;
                var resourceGroupLocation = data.resources[0].location;

                console.log("sourceResourceId = "+JSON.stringify(sourceResourceId));
                console.log("resourceGroupLocation = "+resourceGroupLocation);

                var copyImage_updateRunbookRequest = {
                    resourceGroupName: "Automation",
                    automationAccountName: "General-Automation-Account",
                    runbookName: "copyImage",
                    runbookScriptUri: "https://thirdpartysoftwares.blob.core.windows.net/software/copyImage.ps1"
                };

                var createVMSnapshot_updateRunbookRequest = {
                    resourceGroupName: "Automation",
                    automationAccountName: "General-Automation-Account",
                    runbookName: "createVMSnapshot",
                    runbookScriptUri: "https://thirdpartysoftwares.blob.core.windows.net/software/createVMSnapshot.ps1"
                };

                var createVMSnapshot_Request = {
                    sourceResourceGroupName: request.resourceGroup,
                    sourceVmName: request.vmName,
                    sourceRegion: resourceGroupLocation,
                    targetImageName: request.vmImageName,
                    waitAfterCreation: 5 // minutes
                };

                var copyVHDBlob_Request = {
                    sourceResourceGroupName: request.resourceGroup,
                    sourceVmName: request.vmName,
                    sourceRegion: resourceGroupLocation,
                    targetResourceGroupName: request.targetResourceGroup,
                    targetStorageAccountName: request.targetStorageAccount,
                    targetImageContainerName: request.targetContainer,
                    targetImageName: request.targetImageName,
                    targetImageBlobName: request.targetImageBlobName,
                    snapshotAccessSasUri: ""
                };

                var grantAccessRequest = {
                    resourceGroupName: request.resourceGroup,
                    snapshotName: request.vmImageName + "-" + resourceGroupLocation + "-snap",
                    duration: 7200
                };

                updateRunbook(copyImage_updateRunbookRequest, options, function(err,result) {
                    if (err) callback(err,null);
                    else {
                        updateRunbook(createVMSnapshot_updateRunbookRequest, options, function(err,result) {
                            if (err) callback(err,null);
                            else {
                                VHDCreationWorkflow(callback);
                            }
                        });
                    }
                });

                var VHDCreationWorkflow = function(callback) {
                    createVMSnapshotWithRunbook(createVMSnapshot_Request, options, function(err,result) {
                        if (err) callback(err,null);
                        else {
                            sleep(createVMSnapshot_Request.waitAfterCreation * 60 * 1000).then(function() {
                                grantAccessToSnapshot2(grantAccessRequest, function(err,result) {
                                    if (err) {
                                        console.log("Grant access error = "+err);
                                        callback(err,null);
                                    }
                                    else {
                                        console.log("Snapshot access grant success!");
                                        var accessSasUri = result.stdout.split('"accessSas": "')[1].split('"')[0];
                                        console.log("accessSasUri = "+accessSasUri);
                                        copyVHDBlob_Request.snapshotAccessSasUri = accessSasUri;
                                        sleep(5000).then(function() {
                                            copyVHDBlobWithRunbook(copyVHDBlob_Request,options,callback);
                                        });
                                    }
                                });
                            });
                        }
                    });
                };
                //////////////////////////////////////////////////////////////////////
            };

            // Do Rest GET request to get generalized disk Id
            rest.get(getVMInfoUri, options)
                .on('fail', function(data, response) {
                    callback(data, null);
                })
                .on('success', function(data, response) {
                    createVMDiskWorkflow(data, callback);
                });
        }
    });
};