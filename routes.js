var express = require('express');
var router = express.Router();

var packageCreate = require('./packageCreate');
var auth = require('./authentication');

var helpers = require('./helpers');

var packageCreateJobs = packageCreate.packageCreateJobs;

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

router.all('*', helpers.setCorsHeaders);

router.post('/uploadScriptAsBlob', auth.requiresAuth, function(req,res){
    console.log(req.body);

    console.log('uploading script as file blob ...');

    packageCreate.uploadScriptAsBlob(req.body.scriptName, req.body.scriptPath, 0, function(err,result) {
        if (err) return res.status(500).json({success:false,message:"uploadScriptAsBlob Failed!",result: err});
        else return res.status(200).json({success:true,message:"uploadScriptAsBlob Execution Success!",result: result});
    });

});

router.post('/runCustomScripts', auth.requiresAuth, function(req,res){
    console.log(req.body);

    console.log('setting VM for customScript ...');
    packageCreate.setVMForCustomScript(req.body.resourceGroup,req.body.vmName);

    packageCreate.execCustomBlobScripts(req.body.scripts, function(err,result) {
        if (err) return res.status(500).json({success:false,message:"Scripts Execution Failed!",result: err});
        else return res.status(200).json({success:true,message:"Scripts Execution Success!",result: result});
    });

});

router.post('/sysprepVM', auth.requiresAuth, function(req,res){

    console.log('sysprep-ing VM ...');

    packageCreate.sysprepVM(req.body.resourceGroup, req.body.vmName, function(err,result) {
        if (err) return res.status(500).json({success:false,message:"sysprepVM Failed!",result: err});
        else return res.status(200).json({success:true,message:"sysprepVM Execution Success!",result: result});
    });
});

var apiCalls = require('./apiCalls');

router.post('/copyVHDToSA', auth.requiresAuth, function(req,res){

    console.log('copying generalized VHD to images Storage Account ...');

    console.log("uploading copyImage.ps1 as file blob ...");
    packageCreate.uploadScriptAsBlob("copyImage.ps1", "sysprepVM/copyImage.ps1", 0, function(err,result) {
        if (err) return res.status(500).json({success:false,message:"copyVHDToSA (uploadScriptAsBlob) Failed!",result: err});
        else {
            console.log("uploaded copyImage.ps1 script as blob!");

            console.log("uploading createVMSnapshot.ps1 as file blob ...");
            packageCreate.uploadScriptAsBlob("createVMSnapshot.ps1", "sysprepVM/createVMSnapshot.ps1", 0, function(err,result) {
                if (err) return res.status(500).json({success:false,message:"copyVHDToSA (uploadScriptAsBlob) Failed!",result: err});
                else {
                    console.log("uploaded createVMSnapshot.ps1 script as blob!");
                    apiCalls.createDisk(req.body, function (err, result) {
                        if (err) return res.status(500).json({success: false, message: "copyVHDToSA (apiCalls.createDisk) Failed!", result: err});
                        return res.status(200).json({success: true, message: "copyVHDToSA Execution Success!", result: result});
                    });
                }
            });
        }
    });
});

router.post('/generalizeVM', auth.requiresAuth, function(req,res){

    console.log('generalizing VM ...');

    packageCreate.execGeneralizeVM(req.body, function(err,result) {
        if (err) return res.status(500).json({success:false,message:"generalizeVM Failed!",result: err});
        return res.status(200).json({success:true,message:"generalizeVM Execution Success!",result: result});
    });
});


router.post('/createVMImage', auth.requiresAuth, function(req,res){

    console.log('sysprep-ing VM ...');

    packageCreate.sysprepVM(req.body.resourceGroup, req.body.vmName, function(err,result) {
        if (err) return res.status(500).json({success:false,message:"createVMImage (sysprepVM) Failed!",result: err});
        else {
            console.log('generalizing VM ...');
            packageCreate.execGeneralizeVM(req.body.resourceGroup, req.body.vmName, req.body.vmImageName, function(err,result) {
                if (err) return res.status(500).json({success:false,message:"createVMImage (generalize) Failed!",result: err});
                else return res.status(200).json({success:true,message:"createVMImage Execution Success!",result: result});
            });
        }
    });
});

router.post('/createVM', auth.requiresAuth, function(req,res){

    console.log('creating VM for app installation ...');

    var vmName = req.body.resourceGroup + "vm" + req.body.vmNumber;

    var request = req.body;
    request.vmName = vmName;

    packageCreate.setVMParams(request);

    packageCreate.execCreateVM(req.body.resourceGroup, req.body.datacenter, vmName, function(err,result) {
        if (err) return res.status(500).json({success:false,message:"VM Creation Failed!",result: err});
        else return res.status(200).json({success:true,message:"VM Creation Success!",result: result});
    });

});

var asyncHelpers = require('./asyncHelpers');

router.get('/jobStatus/:jobId', auth.requiresAuth, function(req,res){
    if (req.params.jobId) {
        if (!packageCreateJobs[req.params.jobId]) {
            res.status(404).json({success: false, message: "cannot find jobId " + req.params.jobId});
        } else {
            res.status(200).json({success: true, message: packageCreateJobs[req.params.jobId]});
        }
    }
});

router.post('/createPackage', auth.requiresAuth, function(req,res){
    if ('OPTIONS' === req.method) {
        return;
    }
    var request = req.body;

    if (packageCreateJobs[request.jobId])
        return res.status(403).json({success:false,message:"jobId: "+request.jobId+" already exists in workflow!!"});

    var waitTimes = {};
    if (!request.waitTimes)
        waitTimes = {
            afterCreateVM: 5, // 1 minute
            afterInstallCustomApps: 120,
            afterSysprepVM: 5,
            afterGeneralizeVM: 2,
            afterCopyVHDToSA: 30
        };
    else waitTimes = request.waitTimes;

    packageCreate.setVMParams(request);

    console.log('adding createPackage jobId: "'+request.jobId+'" to work queue ...');
    packageCreateJobs[request.jobId] = {type: "package creation", stage: "/createVM", status: "creating"};
    // Add async printing of job status
    packageCreate.printJobStatus(request.jobId).then(v => { });

    console.log('creating VM for app installation ...');

    var callback = function(err,result) {
        if (err) {
            packageCreateJobs[request.jobId].status = "failed";
            packageCreateJobs[request.jobId].error = err;
            var stage = packageCreateJobs[request.jobId].stage;
            console.log('Error occured at stage "' + stage + '"');
            console.log("Error = " + err);
        }
        else if (result) {
            console.log("Successful workflow callback exec!!");
        } else {
            console.log("This should never occur ...");
        }
    };

    packageCreate.execCreateVM(request.resourceGroup, request.datacenter, request.vmName, function(err,result) {
        if (err) callback(err,null);
        else
        {
            sleep(waitTimes.afterCreateVM * 60 * 1000).then(v => { // minutes * (sec/min) * (sec/ms)
                if (packageCreateJobs[request.jobId].status === "failed") return;
                packageCreateJobs[request.jobId].stage = "/runCustomScripts";
                ////////////////////////////////////////////////////////////////////////////
                console.log("--------- /runCustomScripts ---------");
                asyncHelpers.installCustomApps(request,callback).then(v => {

                    sleep(waitTimes.afterInstallCustomApps * 60 * 1000).then(v => {
                        if (packageCreateJobs[request.jobId].status === "failed") return;
                        packageCreateJobs[request.jobId].stage = "/sysprepVM";
                        ////////////////////////////////////////////////////////////////////////////
                        console.log("--------- /sysprepVM ---------");
                        asyncHelpers.sysprepVM(request,callback).then(v => {
                            sleep(waitTimes.afterSysprepVM * 60 * 1000).then(v => {
                                if (packageCreateJobs[request.jobId].status === "failed") return;
                                packageCreateJobs[request.jobId].stage = "/generalizeVM";
                                ////////////////////////////////////////////////////////////////////////////
                                console.log("--------- /generalizeVM ---------");
                                asyncHelpers.generalizeVM(request,callback).then(v => {
                                    sleep(waitTimes.afterGeneralizeVM * 60 * 1000).then(v => {
                                        if (packageCreateJobs[request.jobId].status === "failed") return;
                                        packageCreateJobs[request.jobId].stage = "/copyVHDToSA";
                                        ////////////////////////////////////////////////////////////////////////////
                                        console.log("--------- /copyVHDToSA ---------");
                                        asyncHelpers.copyVHDToSA(request,callback).then(v => {
                                            sleep(waitTimes.afterCopyVHDToSA * 60 * 1000).then(v => {
                                                if (packageCreateJobs[request.jobId].status !== "failed")
                                                    packageCreateJobs[request.jobId].status = "finished";
                                                ///////////////////////////////////////////////////////////////////////
                                                console.log("/createPackage WORKFLOW FINISHED");
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });

            return;
        }
    });

    sleep(3000).then(function(){
        res.status(200).json({success:true,message:"/createPackage request successful",result: ""});
    });

});

router.post('/createPackage2', auth.requiresAuth, function(req,res){
    if ('OPTIONS' === req.method) {
        return;
    }
    var request = req.body;

    if (packageCreateJobs[request.jobId])
        return res.status(403).json({success:false,message:"jobId: "+request.jobId+" already exists in workflow!!"});

    if (!request.waitTimes)
        request.waitTimes = {
            afterCreateVM: 5, // 1 minute
            afterInstallCustomApps: 120,
            afterSysprepVM: 5,
            afterGeneralizeVM: 2,
            afterCopyVHDToSA: 75
        };

    packageCreate.setVMParams(request);

    console.log('adding createPackage jobId: "'+request.jobId+'" to work queue ...');
    packageCreateJobs[request.jobId] = {type: "package creation", stage: "/createVM", status: "creating"};
    // Add async printing of job status
    packageCreate.printJobStatus(request.jobId).then(v => { });

    asyncHelpers.createPackageWorkflow(request,"execCreateVM",false).then(v => {
        console.log("/createPackage2 WORKFLOW FINISHED");
    });

    sleep(3000).then(function(){
        res.status(200).json({success:true,message:"/createPackage request successful",result: ""});
    });

});

router.get('/', function(req,res){
    res.status(200).json({success:true,message:"Hello World"});
});

module.exports = router;
