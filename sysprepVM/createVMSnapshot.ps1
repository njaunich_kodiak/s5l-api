param (
    [Parameter(Mandatory=$true)]
    [String]$sourceResourceGroupName,

    [Parameter(Mandatory=$true)]
    [String]$sourceVmName,

    [Parameter(Mandatory=$true)]
    [String]$sourceRegion,

    [Parameter(Mandatory=$true)]
    [String]$targetImageName

)

$appId = "3fc44304-5066-4204-bbac-feecc9d2dbf5"
$appSecretPassword = "52401101-1cf0-4fcb-af82-e6371865b1c9"
$tenantId = "c648dcad-f07a-4660-b409-2d57cef5a30a"

$SecurePassword = ConvertTo-SecureString -String $appSecretPassword -AsPlainText -Force

# Convert Service Principal variables into Credential for Login-AzureRmAccount
$credential = New-Object -TypeName System.Management.Automation.PSCredential($appId, $SecurePassword)

# Login to Azure using Service Principal
#Login-AzureRmAccount -ServicePrincipal -ApplicationId  $appId -Credential $credential -TenantId $tenantId
Login-AzureRmAccount -Credential $credential -Tenant $tenantId -ServicePrincipal

<# -- Create a snapshot of the OS (and optionally data disks) from the generalized VM -- #>
$vm = Get-AzureRmVM -ResourceGroupName $sourceResourceGroupName -Name $sourceVmName
$disk = Get-AzureRmDisk -ResourceGroupName $sourceResourceGroupName -DiskName $vm.StorageProfile.OsDisk.Name

$snapshot = New-AzureRmSnapshotConfig -SourceUri $disk.Id -CreateOption Copy -Location $sourceRegion

$snapshotName = $targetImageName + "-" + $sourceRegion + "-snap"

New-AzureRmSnapshot -ResourceGroupName $sourceResourceGroupName -Snapshot $snapshot -SnapshotName $snapshotName