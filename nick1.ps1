<#  
.SYNOPSIS  
    Install TEST application.

.DESCRIPTION
    powershell cmdlet to connect the storage account(thirdpartysoftwares)
    Copying software data(.msi/.zip file) from the storage account 
    Extracting the data from .zip file
    copy the software to the programfiles
    Creating a shortcut of the installed apllication
    Deleting the .zip file
    Disconnecting the storage account.

.EXAMPLE
   ./test.ps1

#> 

net use W: \\thirdpartysoftwares.file.core.windows.net\deployment /u:thirdpartysoftwares iCtU305iytDt53ttlpPcr3QvSEDUHID8+RuUtSfJSHtvn5sMwU6jteWoyCo9Q8uikF+3DmGHRBa6LeKCP/DGuA==
Copy-Item W:\MATLAB.zip  -Destination "C:\" -Recurse

function Expand-ZIPFile($file, $destination)
{
    $shell = new-object -com shell.application
    $zip = $shell.NameSpace($file)
    foreach($item in $zip.items())
    {
        $shell.Namespace($destination).copyhere($item)
    }
}

Expand-ZIPFile -wait –File C:\MATLAB.zip –Destination "C:\Program Files\"

$path = "$env:USERPROFILE\Documents\MATLAB"

If (Test-Path -Path $path -PathType Container)

    { Write-Host "$path already exists" -ForegroundColor Red
}
    ELSE

        { New-Item -Path $path  -ItemType directory 
}

$TargetFile = "C:\Program Files\MATLAB\R2016b\bin\matlab.exe"
$ShortcutFile =  "C:\MATLAB R2016b.lnk"
$WScriptShell = New-Object -ComObject WScript.Shell
$Shortcut = $WScriptShell.CreateShortcut($ShortcutFile)
$Shortcut.TargetPath = $TargetFile
$Shortcut.WorkingDirectory="$env:USERPROFILE\Documents\MATLAB"
$Shortcut.Save()

Remove-Item -Path C:\MATLAB.zip
net use W: /delete Y

param([string[]]$userPrefix,[string[]]$password1)
    $arr = new-object System.Collections.arrayList
    $user=$userPrefix.split(",")
    $password=$password1.split(",")
    $adUser=Get-ADUser -Filter *
    $adUser=$adUser[0].DistinguishedName.split(',')
    $DC1=$adUser[2]
    $DC2=$adUser[3]
    $path= "CN=Users"+","+$DC1+","+$DC2
    $domainName=$DC1.split("=")
    $domainName=$domainName[1]
    $domain=$DC2.split("=")
    $domain=$domain[1]
    $domainName=$domainName+"."+$domain