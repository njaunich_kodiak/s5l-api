#!/bin/sh

. scripts_setup.sh

kubectl create secret docker-registry dockerregistrykey --docker-server=$CURRENT_STRATUS_DOCKER_REGISTRY --docker-username=$APP_ID --docker-password=$APP_SECRET_PASSWORD --docker-email=$EMAIL
#kubectl create secret generic cloud-s5lio-https-privkey --from-file=../../certs/privkey/privkey.pem
#kubectl create secret generic cloud-s5lio-https-fullchain --from-file=../../certs/fullchain/fullchain.pem